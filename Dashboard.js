import React from 'react';
import ReactDOM from 'react-dom';
import Table from 'react-bootstrap/lib/Table';
import Button from 'react-bootstrap/lib/Button';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import PageHeader from 'react-bootstrap/lib/PageHeader';
import Pagination from 'react-bootstrap/lib/Pagination';
import PaginationButton from 'react-bootstrap/lib/PaginationButton';
import {NavItem} from 'react-bootstrap';
import {NavDropdown} from 'react-bootstrap';
import {NavbarBrand} from 'react-bootstrap';
import {Navbar} from 'react-bootstrap';
import {Nav} from 'react-bootstrap';
import {MenuItem} from 'react-bootstrap';
import {Pager} from 'react-bootstrap';
import {Carousel} from 'react-bootstrap';

class Dashboard extends React.Component{
constructor(){
  super();

  this.state = {
    table_data : [
      {id:'1', r_eng: 'Gecko' , browser: 'Firefox 1.0', platform: 'Win 98+/OSX.2+', v_eng: '1.7', css_grade: 'A' },
      {id:'2', r_eng: 'Gecko' , browser: 'Firefox 1.5', platform: 'Win 98+/OSX.2+', v_eng: '1.8', css_grade: 'A' },
      {id:'3', r_eng: 'Gecko' , browser: 'Firefox 2.0', platform: 'Win 98+/OSX.2+', v_eng: '1.8', css_grade: 'A' },
      {id:'4', r_eng: 'Gecko' , browser: 'Firefox 3.0', platform: 'Win 2k+/OSX.3+', v_eng: '1.9', css_grade: 'A' },
      {id:'5', r_eng: 'Gecko' , browser: 'Camino 1.0', platform: 'OSX.2+', v_eng: '1.8', css_grade: 'A' },
      {id:'6', r_eng: 'Gecko' , browser: 'Camino 1.5', platform: 'OSX.3+', v_eng: '1.8', css_grade: 'A' },
      {id:'7', r_eng: 'Gecko' , browser: 'Netscape 7.2', platform: 'Win 95+/Mac OS 8.6-9.2', v_eng: '1.7', css_grade: 'A' },
      {id:'8', r_eng: 'Gecko' , browser: 'Netscape browser 8', platform: 'Win 98SE+', v_eng: '1.7', css_grade: 'A' },
      {id:'9', r_eng: 'Gecko' , browser: 'Netscape Navigator 9', platform: 'Win 98+/OSX.2+', v_eng: '1.8', css_grade: 'A' },
      {id:'10', r_eng: 'Gecko' , browser: 'Mozilla 1.0', platform: 'Win 95+/OSX.1+', v_eng: '1', css_grade: 'A' }
    ]
  }
}
render(){
    let rows = this.state.table_data.map (nova => {
      return <HoverTable key={nova.id} data={nova}/>
    })
    return (
      <div>
          <div className="wrapper">
              <aside className="main-sidebar">
                  <div className="user-panel">
                    <div className="pull-left image">
                      <img src="/assets/background-wallpaper.jpg" className="img-circle" alt="User Image"/>
                    </div>
                    <div className="pull-left info align-center">
                      <div className="margin">MY PROJECT</div>
                    </div>
                  </div>
                  <div className="sidebar-wrapper">
                    <ul className="nav">
                      <li><a className="text-center nav-pills">Hover Table</a></li>
                    </ul>
                  
                    <ul>  
                      <li>
                        <a href="#">
                              <i className="ti-user"></i>
                              <p>User Profile</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                              <i className="ti-mobile"></i>
                              <p>Mobile</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                              <i className="ti-calendar"></i>
                              <p>Calendar</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                              <i className="ti-music"></i>
                              <p>Music</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                              <i className="ti-map"></i>
                              <p>Maps</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                              <i className="ti-key"></i>
                              <p>Encryption Key</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                              <i className="ti-desktop"></i>
                              <p>Desktop</p>
                        </a>
                      </li>
                    </ul>  
                  </div>    
              </aside> 
              
              <div className="main-panel margin-bottom-0">       
                    
                    <div className="container">
                      <Navbar>
                          <Navbar.Header >
                            <Navbar.Brand >
                              <a href="#">Bootstrap Hover Table</a>
                            </Navbar.Brand>
                          </Navbar.Header>
                          <Nav className="pull-right">
                            <NavItem eventKey={1} href="#">Home</NavItem>
                            <NavItem eventKey={2} href="#">About</NavItem>
                            <NavDropdown eventKey={3} title="Contact Us" id="basic-nav-dropdown">
                              <MenuItem eventKey={3.1}>Call</MenuItem>
                              <MenuItem eventKey={3.2}>E-mail</MenuItem>
                              <MenuItem eventKey={3.3}>Fax</MenuItem>                               
                            </NavDropdown>
                          </Nav>
                      </Navbar>
                    </div>
                    <div>  
                      <Carousel>
                          <Carousel.Item className="carousel-item-height">
                            <img width={1100} height={500} alt="900x500" src="/assets/background-wallpaper.jpg"/>
                            <Carousel.Caption>
                              <h3>The idea of waiting for something makes it more fascinating.</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                          <Carousel.Item className="carousel-item-height">
                            <img width={1100} height={500} alt="900x500" src="/assets/background-wallpaper-1.jpg"/>
                            <Carousel.Caption>
                              <h3>Technology over technique produces emotionless design..</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                          <Carousel.Item className="carousel-item-height">
                            <img width={1100} height={500} alt="900x500" src="/assets/background-wallpaper-2.jpg"/>
                            <Carousel.Caption>
                              <h3>Good design is obvious. Great design is transparent.</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                      </Carousel>
                    </div>
                    <div className="text">
                        <h4>HOVER TABLE</h4>
                    </div>
                     
                      <table className="table" >
                          <tbody className="text-center">
                          <tr>  
                              <td><b>Rendering Engine</b></td>
                              <td><b>#</b></td>
                              <td><b>Browser</b></td>
                              <td><b>Platforms</b></td>
                              <td><b>Engine version</b></td>
                              <td><b>CSS Grade</b></td>
                          </tr>
                              {rows}
                      </tbody>
                  </table>

                  <div id="example" className="pull-right">
                     
                            <Pagination
                               
                               first='Previous'
                               last='Next'
                               items={7}
                               maxButtons={7} 
                               onSelect={
                                () => {
                                  console.log("Value of this", this)
                                     }
                                  } 
                            />                       
                  </div>
              </div>
          </div>                            
      </div>
    );
  }
}
const HoverTable = (props) => {
  return  <tr>
    <td>{props.data.id}.</td>
    <td>{props.data.r_eng}</td>
    <td>{props.data.browser}</td>
    <td>{props.data.platform}</td>
    <td>{props.data.v_eng}</td>
    <td>{props.data.css_grade}</td>
    </tr>
}
export default Dashboard