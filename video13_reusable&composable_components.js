import React from 'react';
import ReactDOM from 'react-dom';
class App extends React.Component{

  constructor(){
    super();
    this.state = {
      value: 0
    }
    this.update= this.update.bind(this)
  }

  update(){
    this.setState({
      value: ReactDOM.findDOMNode(this.refs.red.refs.inp).value
    })
  }
  render(){
    return (
      <div>
      <NumInput ref= "red"
        min={0}
        max={255}
        step={1}
        val={this.state.value}
        label="Red"
        type="range"
        update = {this.update}/>
       </div>
    );
  }
}


class NumInput extends React.Component{
  render(){
    let label = this.props.label !== '' ?
      <label>{this.props.label} - {this.props.val}</label> : ''
    return(
        <div>
            <input ref= "inp"
              type= {this.props.type}
              min= {this.props.min}
              max= {this.props.max}
              step= {this.props.step}
              defaultValue= {this.props.value}
              onChange={this.props.update} />
            {label}
        </div>
      );
  }
}

NumInput.propTypes = {
  min: React.PropTypes.number,
  max: React.PropTypes.number,
  val: React.PropTypes.number,
  label: React.PropTypes.string,
  step: React.PropTypes.number,
  update: React.PropTypes.func.isRequired,
  type: React.PropTypes.oneOf(['number','range'])
}
NumInput.defaultProps = {
  min: 0,
  max: 0,
  val: 0,
  label: '',
  step: 0,
  type: 'range'
}

export default App
