var Redux = require('redux');
var React = require('react');
var ReactDOM = require('react-dom');
const { Component } = React;
const {createStore} = Redux;
const { combineReducers } = Redux;
let nextTodoId=0;

class TodoApp extends Component {
	render() {
		const visibleTodo = getVisibleTodo(this.props.todos, this.props.visibilityFilter);
		console.log('my value for visible tto', visibleTodo);
		return(
			<div>
				<input ref={ node => {
					this.input = node;
				}} />
				<button onClick={() => {
					store.dispatch({
						type: 'ADD_TODO',
						text: this.input.value,
						id: nextTodoId++
					});
					this.input.value=' ';
				}}>
					Add Todo
				</button>
				<ul>
				 	{visibleTodo.map(todo =>
				 		<li key={todo.id} 
								onClick = { () => {
				 					store.dispatch({
				 						type: 'TOGGLE_TODO',
				 						id: todo.id
				 					});
				 				}}
				 			style={{
				 				textDecoration: todo.completed ? 'line-through' : 'none'
				 			}}
				 		>
				 			{todo.text}
				 		</li>	
				 	)}
				</ul>
				<p>
				 	Show:
				 	{' '}
				 	<FilterLink filter="SHOW_ALL" currentfilter= {this.props.visibilityFilter}>All</FilterLink>
				 	<FilterLink filter="SHOW_ACTIVE" currentfilter= {this.props.visibilityFilter}> Active</FilterLink>
				 	<FilterLink filter="SHOW_COMPLETED" currentfilter= {this.props.visibilityFilter}> Completed</FilterLink>
				</p>
			</div>
		);
	}
}

const todos = ( state=[], action) => {
	switch (action.type) {
		case 'ADD_TODO' :
			return [
				...state,
				todo( undefined, action)
			];
		case 'TOGGLE_TODO':

			return state.map(t =>
				todo(t, action)
			);
		default:
			return state;
	}
};
const visibilityFilter = (state= 'SHOW_ALL', action) => {
	switch (action.type) {
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
};
const todo =( state, action) => {
	switch (action.type) {
		case 'ADD_TODO':
			return {
				id: action.id,
				text: action.text,
				completed: false
			};
		case 'TOGGLE_TODO':
			if (state.id !== action.id) {
				return state;
			}
			return{
				...state,
				completed: !state.completed
			};
		default:
			return state;
	}
};


const FilterLink = ({filter,children, currentfilter}) => {
	if (filter === currentfilter){
			return <span>{children}</span>
		}
	return(
		<a href="#" 
			onClick={ (e) =>{
				e.preventDefault();
				store.dispatch({
					type: 'SET_VISIBILITY_FILTER',
					filter
				});
			}}
		>
		{children}
		</a>
	);
};
const getVisibleTodo = (todos, filter) => {
	switch (filter){
		case 'SHOW_ALL':
			return todos;
		case 'SHOW_ACTIVE':
			return todos.filter(t=> !t.completed);
		case 'SHOW_COMPLETED':
			return todos.filter(t=> t.completed);
	}
};

const todoApp = combineReducers({todos, visibilityFilter});
const store = createStore(todoApp);


const render = () => {
	ReactDOM.render(
		<TodoApp 
		{...store.getState()}/>,
		document.getElementById('root')
		)
};

store.subscribe(render);
render();