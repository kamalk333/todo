import React from 'react';
import ReactDOM from 'react-dom';
class App extends React.Component{

  constructor(){
    super();
    this.state = {
      data: [
        {id:1, name: "Kamal Kumawat"},
        {id:2, name: "Dharmesh Patel"},
        {id:3, name: "Ankur Patel"},
        {id:4, name: "Akash paradkar"},
        {id:5, name: "Advait Save"},
        {id:6, name: "Akshay Pasad"},
        {id:7, name: "Sundar"},
        {id:8, name: "Chinmay Rawool"}
      ]

    }
}
  render(){
    let rows = this.state.data.map (person => {
      return <PersonRow key={person.id} data={person}/>
    })
    return (
      <table>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }
}
const PersonRow = (props) => {
  return <tr>
    <td>{props.data.id}.</td>
    <td>{props.data.name}</td>
    </tr>
}
export default App
